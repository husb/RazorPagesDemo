using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json.Linq;

namespace MvcClient.Pages
{
    public class CallApiModel : PageModel
    {
        public string Message { get; set; }

        public string Json { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        private readonly IHttpClientFactory _httpClientFactory;

        public CallApiModel(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        //public void OnGet()
        public async Task<IActionResult> OnGetAsync()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");

            var client = _httpClientFactory.CreateClient();
            //client.BaseAddress = new Uri("http://api.github.com");
            //string result = await client.GetStringAsync("/");

            //var client = new HttpClient();
            //client.SetBearerToken(accessToken);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var content = await client.GetStringAsync("https://localhost:5001/api/values");

            Message =  "Your application description page.";

            Json = JArray.Parse(content).ToString();

            RefreshToken = await HttpContext.GetTokenAsync("refresh_token");
            AccessToken = accessToken;

            return Page();
        }
    }
}