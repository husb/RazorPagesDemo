﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace RazorPagesDemo.Pages
{
    public class IndexModel : PageModel
    {

        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<IndexModel> _logger;
        public IndexModel(IServiceProvider provider, ILogger<IndexModel> logger)
        {
            _serviceProvider = provider;
            _logger = logger;
        }

        public void OnGet()
        {
            var profileServices = _serviceProvider.GetServices<IProfileService>();
            var ss = "";
            foreach (var s in profileServices)
            {
                ss += s.GetType().FullName + Environment.NewLine;
            }
            var validators = _serviceProvider.GetServices<IResourceOwnerPasswordValidator>();
            foreach (var s in validators)
            {
                ss += s.GetType().FullName + Environment.NewLine;
            }
        }
    }
}
